import React, { Component } from "react";

const optionsMenu = [
  "Nuevo grupo",
  "Perfil",
  "Archivados",
  "Destacados",
  "Configuración",
  "Cerrar sesión",
];
class MenuChat extends Component {
  menu = React.createRef();

  componentDidUpdate() {
    this.menu.current.classList.toggle("ocultarMenu", !this.props.stateMenu);
  }
  render() {
    return (
      <div className="menuChat ocultarMenu" ref={this.menu}>
        <ul className="menulist">
          {optionsMenu.map((option) => (
            <li key={option} className="optionsmenu" title={option}>
              <span>{option}</span>
            </li>
          ))}
        </ul>
      </div>
    );
  }
}
export default MenuChat;
