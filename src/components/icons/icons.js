import bullbasur from "./bullbasaur.png";
import estrella from "./estrella.png";
import meowth from "./meowth.png";
import psy from "./psyduck.png";
import snorlac from "./snorlax (1).png";
import asdas from "./snorlax.png";
import tgth from "./zubat (1).png";
import csds from "./zubat.png";

const icons = [bullbasur, snorlac, csds, estrella, meowth, tgth, psy, asdas];

export default icons;
