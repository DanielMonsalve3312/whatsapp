import React from "react";
import { IconButton, Avatar, Menu, MenuItem } from "@material-ui/core";
import { MoreVert } from "@material-ui/icons";
import contactosUser, { diasSemana } from "./contactosPhone";
import InfoContacto from "./InfoContacto";

let mensaje = [];
const hora = new Date();
const obtionsMenu = [
  "Info. de contacto",
  "Seleccionar mensajes",
  "Silenciar notificaciones",
  "Baciar mensajes",
  "Eliminar chat",
];

class NuevaConversacion extends React.Component {
  constructor() {
    super();
    this.state = {
      texto: "",
      enviar: false,
      visible: true,
      usuario: "",
      anchorEl: null,
    };
  }

  componentDidUpdate() {
    const { userName } = this.props;
    if (userName !== this.state.usuario) {
      this.setState({ usuario: userName });
      mensaje = [];
    }
  }

  enviarMensaje = () => {
    mensaje.unshift(this.state.texto);
    this.setState({ enviar: true, texto: "", visible: true });
  };

  handleChange = (e) => {
    this.setState({
      texto: e.target.value,
    });
    if (e.target.value !== "") {
      this.setState({ visible: false });
    } else {
      this.setState({ visible: true });
    }
  };

  enterPress = (e) => {
    if (e.key === "Enter" && this.state.texto !== "") {
      this.enviarMensaje();
    }
  };

  mostrarMenu = (e) => {
    this.setState({ anchorEl: e.target });
  };
  ocultMenu = (e) => {
    this.setState({ anchorEl: null });
  };

  render() {
    const { data, userId, avatar, abrirOrCerrarInfo, infoContact } = this.props;
    const allContacts = data.concat(contactosUser);

    return (
      <React.Fragment>
        <div className="containert">
          <header className="header">
            <div className="options">
              <div
                className="infoContact"
                onClick={() => {
                  abrirOrCerrarInfo(true);
                }}
              >
                <div className="perfilIcon">
                  <Avatar
                    src={avatar}
                    style={{ backgroundColor: allContacts[userId].color }}
                  >
                    {allContacts[userId].name.charAt(0)}
                  </Avatar>
                </div>
                <div className="nameUser" title="Daniel David">
                  <h4>{allContacts[userId].name}</h4>
                </div>
              </div>
              <div className="setthings">
                <IconButton>
                  <i className="material-icons iconLupa" title="Buscar...">
                    search
                  </i>
                </IconButton>
                <IconButton>
                  <i className="material-icons clic" title="Adjuntar">
                    attach_file
                  </i>
                </IconButton>
                <IconButton title="Menú" onClick={this.mostrarMenu}>
                  <MoreVert />
                </IconButton>
                <Menu
                  open={Boolean(this.state.anchorEl)}
                  anchorEl={this.state.anchorEl}
                  className="menu"
                  onClose={this.ocultMenu}
                >
                  {obtionsMenu.map((obtion) => (
                    <MenuItem
                      key={obtion}
                      title={obtion}
                      style={{ fontFamily: "Segoe UI" }}
                    >
                      {obtion}
                    </MenuItem>
                  ))}
                </Menu>
              </div>
            </div>
          </header>

          <div className="mensajes">
            <div className="sectionMensage">
              {this.state.enviar
                ? mensaje.map((element) => (
                    <div className="newMensaje" key={Math.random()}>
                      <div className="text">
                        <p>{element}</p>
                      </div>
                      <div className="visto">
                        <p>12:00 p. m.</p>
                        <i className="material-icons">done_all</i>
                      </div>
                    </div>
                  ))
                : null}
              <div className="info">
                <p>
                  <i className="material-icons">lock</i>
                  Las llamadas y mensajes enviados a este chat están protegidos
                  con cifrado de extremo a extremo. Haz clic para más
                  información.
                </p>
              </div>
              <div className="dia">
                <p>{diasSemana[hora.getDay()]}</p>
              </div>
            </div>
            <div className="fondo"></div>
          </div>

          <footer className="escribirMensaje">
            <div className="necesari">
              <div className="iconos">
                <i className="material-icons">mood</i>
              </div>
              <div className="nuevoMensaje">
                <div className="textField">
                  {this.state.visible ? (
                    <label>Escribe un mensaje aquí</label>
                  ) : null}

                  <input
                    onChange={this.handleChange}
                    value={this.state.texto}
                    onKeyDown={this.enterPress}
                    name="texto"
                  />
                </div>
              </div>
              <div className="iconos">
                {this.state.visible ? (
                  <i className="material-icons">mic</i>
                ) : (
                  <i className="material-icons" onClick={this.enviarMensaje}>
                    send
                  </i>
                )}
              </div>
            </div>
          </footer>
        </div>
        {infoContact ? (
          <InfoContacto
            contacto={allContacts[userId]}
            avatar={avatar}
            abrirOrCerrarInfo={abrirOrCerrarInfo}
          />
        ) : null}
      </React.Fragment>
    );
  }
}
export default NuevaConversacion;
