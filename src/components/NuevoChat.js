import React from "react";
import BarraSeacrh from "./BarraSeacrh";
import { Avatar } from "@material-ui/core";
import GroupAddIcon from "@material-ui/icons/GroupAdd";
import contactosPhone from "./contactosPhone";

function NuevoChat(props) {
  const [searchValue, setSearchValue] = React.useState("");
  const [data, setData] = React.useState(contactosPhone);

  const handleChange = (e) => {
    setSearchValue(e.target.value);
  };

  React.useMemo(() => {
    const datos = contactosPhone.filter((element) => {
      return element.name.toLowerCase().includes(searchValue.toLowerCase());
    });
    setData(datos);
  }, [searchValue]);

  return (
    <React.Fragment>
      <div className="title">
        <div className="newChat">
          <i className="material-icons" onClick={props.ocultarChat}>
            arrow_back
          </i>
          <h3>Nuevo chat</h3>
        </div>
      </div>
      <BarraSeacrh
        value={searchValue}
        setValue={setSearchValue}
        funcion={handleChange}
      />

      <div className="contactsPhone">
        {data.length !== 0 ? (
          <React.Fragment>
            <div className="nuevoGrupo">
              <div className="avatar">
                <Avatar className="avatarGruop">
                  <GroupAddIcon />
                </Avatar>
              </div>
              <h3>Nuevo grupo</h3>
            </div>
            <h2 className="frecuentes">FRECUENTES</h2>
            <ul className="ulContacts">
              {data.map((contacto) => (
                <li
                  className="listContactos"
                  key={contacto.id}
                  onClick={(e) => {
                    props.abrirChat(
                      e.currentTarget,
                      e.target,
                      "",
                      contacto.name
                    );
                    props.ocultarChat(e);
                    props.cerrarInfo();
                  }}
                  id={contacto.id - 1}
                >
                  <div className="avatar">
                    <Avatar
                      className="avatarGruop"
                      style={{ backgroundColor: contacto.color }}
                    >
                      {contacto.name.charAt(0)}
                    </Avatar>
                  </div>

                  <div className="datosContact">
                    <h2>{contacto.name}</h2>
                    <p>{contacto.estado}</p>
                  </div>
                </li>
              ))}
            </ul>
          </React.Fragment>
        ) : (
          <div className="notResults">
            <p>No se encontraron resultados para '{searchValue}'</p>
          </div>
        )}
      </div>
    </React.Fragment>
  );
}

export default NuevoChat;
