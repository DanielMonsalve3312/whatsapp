import React, { useState } from "react";
import { Avatar, IconButton } from "@material-ui/core";
import MenuChat from "./MenuChat";
import { MoreVert, DonutLarge } from "@material-ui/icons";
import BarraSeacrh from "./BarraSeacrh";
import ContactList from "./ContactList";
import conection from "./images/conection.jpg";
import NuevoChat from "./NuevoChat";
import NuevaConversacion from "./NuevaConversacion";
import perfil from "./images/perfil.jpg";
import Perfil from "./Perfil";
import ContextAll from "../context/contextAnimations";

function ChatUser(props) {
  const animation = React.useContext(ContextAll);

  const nuevoChat = React.createRef();
  const [infoContact, setInfoContact] = useState(false);
  const [estados, setEstados] = useState({
    primerHijo: false,
    menuState: false,
    barraSeacrh: true,
  });

  const [textValue, setTextValue] = useState("");
  const [data, setData] = useState(props.contacts);
  const [visible, setVisible] = useState(false);
  const [chat, setChat] = useState({
    abierto: false,
    userID: "",
    avatar: "",
    userName: "",
  });

  //Ventana de la izquierda
  const mostrarPerfil = () => {
    animation.mostrar(setVisible);
    setEstados({ primerHijo: false });
    setTextValue("");
  };

  const mostrarContactos = (e) => {
    animation.mostrar(setVisible);
    setEstados({ primerHijo: true, barraSeacrh: false });
    setTextValue("");
  };

  const cerrarPerfilOrContactos = () => {
    animation.ocultar(nuevoChat, setVisible, "atras .35s ease-in", 350);
    setEstados({ primerHijo: estados.primerHijo, barraSeacrh: true });
  };

  //Menu Header
  const mostartMenu = () => {
    setEstados({ menuState: true, barraSeacrh: estados.barraSeacrh });
  };
  const ocultarMenu = () => {
    setEstados({ menuState: false, barraSeacrh: estados.barraSeacrh });
  };

  //Valor del Input de BarraSearch
  const handleChange = (e) => {
    setTextValue(e.target.value);
  };

  //Contacto para mensajes
  const abrirChat = (e, elemnt, avatar, name) => {
    if (elemnt.id === "") {
      setChat({
        abierto: true,
        userID: e.id,
        avatar: avatar,
        userName: name,
      });
    }
  };

  const cerrarInfo = () => {
    setInfoContact(false);
  };

  //Filtro de contactos
  React.useMemo(() => {
    const datos = props.contacts.filter((element) => {
      return element.name.toLowerCase().includes(textValue.toLowerCase());
    });
    setData(datos);
  }, [textValue, props.contacts]);

  return (
    <React.Fragment>
      <div className="father">
        <div className="contactos">
          {visible ? (
            <div className="atras" ref={nuevoChat}>
              {estados.primerHijo ? (
                <div className="nuevoChat">
                  <NuevoChat
                    ocultarChat={cerrarPerfilOrContactos}
                    abrirChat={abrirChat}
                    cerrarInfo={cerrarInfo}
                  />
                </div>
              ) : (
                <div className="perfil">
                  <Perfil ocultarChat={cerrarPerfilOrContactos} src={perfil} />
                </div>
              )}
            </div>
          ) : null}
          <div className="principal">
            <div className="secundario">
              <header className="header">
                <div className="avatar">
                  <Avatar
                    className="perfil"
                    src={perfil}
                    onClick={mostrarPerfil}
                  />
                </div>
                <div className="obtions">
                  <IconButton title="Estados" id="hola">
                    <DonutLarge />
                  </IconButton>

                  <IconButton title="Nuevo chat" onClick={mostrarContactos}>
                    <i className="material-icons">chat</i>
                  </IconButton>
                  <IconButton
                    title="Menú"
                    onClick={mostartMenu}
                    onBlur={ocultarMenu}
                  >
                    <MoreVert />
                  </IconButton>
                </div>
                <MenuChat stateMenu={estados.menuState} />
              </header>

              {estados.barraSeacrh ? (
                <BarraSeacrh
                  value={textValue}
                  setValue={setTextValue}
                  funcion={handleChange}
                />
              ) : (
                <div className="search"></div>
              )}
              <div className="chat">
                {data.length === 0 ? (
                  <div className="noTcontac">
                    <p>No se encontró nigun chát, contacto ni mensaje</p>
                  </div>
                ) : (
                  <ContactList
                    data={data}
                    abrirChat={abrirChat}
                    cerrarInfo={cerrarInfo}
                  />
                )}
              </div>
            </div>
          </div>
        </div>
        {chat.abierto ? (
          <div className="todo">
            <NuevaConversacion
              userId={chat.userID}
              data={props.contacts}
              avatar={chat.avatar}
              userName={chat.userName}
              abrirOrCerrarInfo={setInfoContact}
              infoContact={infoContact}
            />
          </div>
        ) : (
          <div className="combersacion">
            <div className="info">
              <div className="information">
                <img src={conection} alt="Conection" />
                <h1>Mantén tu teléfono conectado</h1>
                <p className="info">
                  WhatsApp se conecta a tu teléfono para sincronizar los
                  mensajes. Para reducir el consumo de tus datos, conecta tu
                  teléfono a una red Wi-Fi.
                </p>{" "}
                <div className="mac">
                  <i className="material-icons">laptop_mac</i>
                  <p>
                    WhatsApp está disponible para Windows.
                    <span>Obtenlo aquí</span>
                  </p>
                </div>
              </div>
            </div>
          </div>
        )}
      </div>
    </React.Fragment>
  );
}

export default ChatUser;
