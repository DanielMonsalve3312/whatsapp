import React from "react";
import { Avatar } from "@material-ui/core";

function Perfil(props) {
  return (
    <React.Fragment>
      <div className="title">
        <div className="newChat">
          <i className="material-icons arrow" onClick={props.ocultarChat}>
            arrow_back
          </i>
          <h3>Perfil</h3>
        </div>
      </div>
      <div className="perfilItachi">
        <div className="perfilPhoto">
          <Avatar className="avatar" src={props.src}></Avatar>
          <div className="foto">
            <div className="hover">
              <i className="material-icons">camera_alt</i>
              <p>CAMBIAR FOTO DE PERFIL</p>
            </div>
          </div>
        </div>
      </div>
      <div className="yourName">
        <p>Tu nombre</p>
        <div className="nombre">
          <h3>Aragon</h3>
          <i className="material-icons" title="Editar">
            create
          </i>
        </div>
      </div>
      <div className="nombrePerfil">
        <p>
          Este no es tu nombre de usuario ni PIN. Este nombre será visible para
          tus contactos de WhatsApp.
        </p>
      </div>
      <div className="yourName">
        <p>Info.</p>
        <div className="nombre">
          <h3>Aragon</h3>
          <i className="material-icons" title="Editar">
            create
          </i>
        </div>
      </div>
    </React.Fragment>
  );
}

export default Perfil;
