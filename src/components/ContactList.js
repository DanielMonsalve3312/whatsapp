import React, { useState } from "react";
import { Avatar, Menu, MenuItem } from "@material-ui/core";
import icons from "./icons/icons";

const menuObtions = [
  "Archivar chat",
  "Silenciar notificaciones",
  "Eliminar chat",
  "Fijar chat",
  "Marcar como no leido",
];

function ContactList(props) {
  const element = React.createRef();
  const [anchorEl, setAnchorrEl] = useState(null);
  const open = Boolean(anchorEl);
  const hora = new Date();

  const expandVista = (e) => {
    setAnchorrEl(e.target);
  };
  const ocultMenu = (e) => {
    setAnchorrEl(null);
  };
  return (
    <React.Fragment>
      <ul className="listaDeContacts">
        {props.data
          ? props.data.map((contacto) => (
              <li
                className="vistaContacts"
                key={contacto.id}
                onClick={(e) => {
                  props.abrirChat(
                    e.currentTarget,
                    e.target,
                    icons[contacto.id - 1],
                    contacto.name
                  );
                  props.cerrarInfo();
                }}
                id={contacto.id - 1}
              >
                <div className="daniel">
                  <div className="avatar">
                    <Avatar
                      className="perfil"
                      src={icons[contacto.id - 1]}
                    ></Avatar>
                  </div>
                  <div className="datosUser">
                    <div className="datosContact">
                      <h3>{contacto.name}</h3>
                      <p>
                        {hora.getHours()}:{hora.getMinutes()} p. m.
                      </p>
                    </div>
                    <div className="date">
                      <p className="recado">{contacto.address.city}</p>
                      <span>
                        <p>{contacto.id}</p>
                        <i
                          className="material-icons"
                          onClick={expandVista}
                          onBlur={ocultMenu}
                          ref={element}
                          id={contacto.id}
                        >
                          expand_more
                        </i>
                      </span>
                    </div>
                  </div>
                </div>
              </li>
            ))
          : null}
      </ul>
      <Menu
        open={open}
        anchorEl={anchorEl}
        className="menu"
        onClose={ocultMenu}
      >
        {menuObtions.map((obtion) => (
          <MenuItem
            key={obtion}
            title={obtion}
            style={{ fontFamily: "Segoe UI" }}
          >
            {obtion}
          </MenuItem>
        ))}
      </Menu>
    </React.Fragment>
  );
}

export default ContactList;
