import React from "react";

function BarraSeacrh(props) {
  const all = React.createRef();
  const input = React.createRef();
  const arrow = React.createRef();

  const [efecto, setEfecto] = React.useState(false);

  const inputsWrite = () => {
    setEfecto(false);
    all.current.classList.replace("active", "disabled");
  };
  const handleFocus = (e) => {
    setEfecto(true);
    input.current.focus();
    all.current.classList.replace("disabled", "active");
  };
  const hancleBlur = (e) => {
    if (props.value === "") {
      inputsWrite();
    }
  };
  const clearInput = () => {
    input.current.focus();
    props.setValue("");
  };
  return (
    <React.Fragment>
      <div className="search disabled" ref={all}>
        <div className="searchChat">
          <span className="iconos">
            {efecto ? (
              <i
                onClick={hancleBlur}
                className="material-icons arrow"
                ref={arrow}
              >
                arrow_back
              </i>
            ) : (
              <i className="material-icons iconLupa" onClick={handleFocus}>
                search
              </i>
            )}
          </span>
          <span className="campoTexto">
            {!efecto ? <label>Buscar o empezar un chat nuevo</label> : null}
            <input
              ref={input}
              value={props.value}
              type="text"
              onChange={props.funcion}
              onFocus={handleFocus}
            />
          </span>
          {efecto ? (
            <i className="material-icons close" onClick={clearInput}>
              close
            </i>
          ) : null}
        </div>
      </div>
    </React.Fragment>
  );
}

export default BarraSeacrh;
