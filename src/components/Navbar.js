import React from "react";
import "./sass/main.css";
import { IconButton } from "@material-ui/core";
import CloseIcon from "@material-ui/icons/Close";
import logo from "./images/logoSmall.svg";
import { withRouter } from "react-router";

function Navbar(props) {
  const handleClick = () => {
    bar.current.classList.remove("oculto");
  };
  const close = () => {
    bar.current.classList.add("oculto");
  };
  const bar = React.createRef();

  if (props.location.pathname === "/chat") {
    return null;
  }

  return (
    <React.Fragment>
      <header className="headerBar">
        <div className="barraNav">
          <a href="/">
            <div className="divImage"></div>
          </a>
          <div className="contNav oculto" ref={bar}>
            <div className="icons-nav">
              <img src={logo} alt="logo" />
              <IconButton onClick={close}>
                <CloseIcon className="close" />
              </IconButton>
            </div>
            <nav className="navigation">
              <ul className="listUl">
                <li className="web">
                  <a href="/chat">whatsapp web</a>
                </li>
                <li>
                  <a href="#i">funciones</a>
                </li>
                <li>
                  <a href="#c">descargar</a>
                </li>
                <li>
                  <a href="#a">seguridad</a>
                </li>
                <li>
                  <a href="#p">preguntas frecuantes</a>
                </li>
                <li className="web">0-Es^</li>
                <li className="contact">
                  <a href="#sd">contáctanos</a>
                </li>
              </ul>
              <select defaultValue="Español" className="selection">
                <option>Español</option>
                <option>English</option>
                <option>日本人</option>
                <option>русский</option>
              </select>
            </nav>
          </div>
          <div className="bt-icon">
            <IconButton onClick={handleClick}>
              <i className="material-icons">menu</i>
            </IconButton>
          </div>
        </div>
      </header>
    </React.Fragment>
  );
}

export default withRouter(Navbar);
