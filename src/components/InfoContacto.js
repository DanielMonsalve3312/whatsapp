import React from "react";
import { Avatar } from "@material-ui/core";
import { CheckBox } from "@material-ui/icons";

function InfoContacto(props) {
  return (
    <div className="infoUser">
      <header className="header">
        <div className="infoContacto">
          <span>
            <i
              className="material-icons"
              onClick={() => {
                props.abrirOrCerrarInfo(false);
              }}
            >
              close
            </i>
          </span>
          <div className="title">
            <p>Info. del contacto</p>
          </div>
        </div>
      </header>
      <div className="datosContacto">
        <div className="avatarContac">
          <div className="perfil">
            <Avatar
              className="avatar"
              src={props.avatar}
              style={{
                backgroundColor: props.contacto.color,
                fontSize: "120px",
              }}
            >
              {props.contacto.name.charAt(0)}
            </Avatar>
          </div>
          <h3>{props.contacto.name}</h3>
        </div>
        <div className="documentos">
          <div className="enlaces">
            <p>Archivos, enlaces y documentos</p>
            <i className="material-icons">navigate_next</i>
          </div>

          <div className="sinArchivos">
            <p>Sin archivos multimedia, enlaces ni documentos</p>
          </div>
        </div>
        <div className="notifcaciones">
          <div className="silenciar">
            <h3>Silenciar notificaciones</h3>
            <CheckBox className="icon" color="primary" />
          </div>
          <div className="destacados">
            <h3>Mensajes destacados</h3>
            <i className="material-icons icon">navigate_next</i>
          </div>
        </div>
        <div className="infoPhone">
          <div className="phone">
            <p>Info. y número de telefono</p>
            <h3>{props.contacto.estado}</h3>
          </div>
          <div className="numPhone">
            <h3>+58 {props.contacto.address.zipcode}</h3>
          </div>
        </div>
        <div className="bloquear">
          <i className="material-icons">do_not_disturb_alt</i>
          <h3>Bloquear</h3>
        </div>
        <div className="reportar">
          <i className="material-icons">thumb_down</i>
          <h3>Reportar chat</h3>
        </div>
        <div className="eliminar">
          <i className="material-icons">delete</i>
          <h3>Eliminar chat</h3>
        </div>
      </div>
    </div>
  );
}

export default InfoContacto;
