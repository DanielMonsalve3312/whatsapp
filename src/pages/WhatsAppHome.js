import React from "react";
import "./styles/mainPages.css";
import phone1 from "./images/phonePage1.png";
import phone2 from "./images/phone2Page1.png";
import AndroidIcon from "@material-ui/icons/Android";
import DesktopMacIcon from "@material-ui/icons/DesktopMac";
import AppleIcon from "@material-ui/icons/Apple";

function WhatsAppHome(props) {
  return (
    <React.Fragment>
      <div className="section1">
        <div className="contMenssge">
          <div className="menssage">
            <div className="cdm">
              <h1 className="title">
                Mensajería confiable.
                <br /> Simple. Segura.
              </h1>

              <p className="callMenssage">
                Con WhatsApp, la mensajería y las llamadas son rápidas, simples,
                seguras y gratuitas*, disponibles en teléfonos alrededor del
                mundo.
              </p>
              <p className="cargos">
                * Puede haber cargos adicionales por el uso del servicio de
                datos. Contacta a tu operador de telefonía móvil para más
                información.
              </p>
              <div className="googlePlay"></div>
              <div className="platafors">
                <ul>
                  <li>
                    <AndroidIcon className="a" />
                    Android
                  </li>
                  <li>
                    <AppleIcon className="a" />
                    Iphone
                  </li>
                  <li>
                    <DesktopMacIcon className="a" />
                    Mac o Windows
                  </li>
                </ul>
              </div>
            </div>
          </div>
          <div className="menssageImage">
            <img src={phone1} alt="" />
          </div>
        </div>
      </div>
      <div className="section2">
        <div className="aplicacion">
          <div className="cargosAdd">
            <p>
              * Puede haber cargos adicionales por el uso del servicio de datos.
              Contacta a tu operador de telefonía móvil para más información.
            </p>
          </div>

          <div className="info">
            <h2>Aplicación WhatsApp Business</h2>
            <p>
              <span>WhatsApp Business</span> es una aplicación que se puede
              descargar de forma gratuita y se diseñó pensando en las pequeñas
              empresas y negocios. Crea un catálogo para mostrar tus productos y
              servicios. Comunícate fácilmente con los clientes mediante
              herramientas que te permitan automatizar, ordenar y responder
              mensajes de forma rápida.
            </p>
            <p>
              WhatsApp también puede ayudar a las pequeñas y medianas empresas a
              brindar soporte técnico a sus clientes y a enviarles
              notificaciones importantes. Obtén más información acerca de la{" "}
              <span>API de WhatsApp Business.</span>
            </p>
          </div>
          <img src={phone2} alt="Phone2" />
        </div>
        <div className="seguridad">
          <div className="seg">
            <div className="none" />
            <p className="sifra">CIFRADO DE EXTREMO A EXTREMO </p>
            <h2>Seguridad automática</h2>
            <p className="tema">
              Algunos de tus momentos más personales se comparten a través de
              WhatsApp; es por ello que desarrollamos el cifrado de extremo a
              extremo en las versiones más recientes de nuestra aplicación. Con
              el cifrado de extremo a extremo, tus mensajes y llamadas están
              protegidos para que solo las personas con las que te comunicas los
              puedan leer o escuchar sin que nadie más, ni siquiera WhatsApp, lo
              pueda hacer.
            </p>
          </div>
        </div>
      </div>
      <div className="funcones">
        <a href="/">EXPLORA FUNCIONES</a>
      </div>
    </React.Fragment>
  );
}

export default WhatsAppHome;
