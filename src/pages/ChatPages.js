import React, { Component } from "react";
import "./styles/mainPages.css";
import ChatUser from "../components/ChatUser";
import Loader from "../components/Loader";

class ChatPages extends Component {
  state = {
    contactos: [],
    error: null,
    loading: true,
  };
  componentDidMount() {
    this.contactsFeatch();
  }
  contactsFeatch = async () => {
    try {
      const contacts = await fetch(
        "https://jsonplaceholder.typicode.com/users"
      );
      const data = await contacts.json();
      this.setState({ contactos: data, loading: false });
    } catch (error) {
      this.setState({ error: error, loading: false });
    }
  };
  render() {
    if (this.state.loading) {
      return <Loader />;
    }
    if (this.state.error) {
      return `Error: ${this.state.error.message}`;
    }
    return <ChatUser contacts={this.state.contactos} />;
  }
}

export default ChatPages;
