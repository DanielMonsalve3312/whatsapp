import React from "react";

export class animation extends React.Component {
  mostrar(setVisible) {
    setVisible(true);
  }
  ocultar(ref, setVisible, animation, delay) {
    ref.current.style.animation = animation;
    setTimeout(() => {
      setVisible(false);
    }, delay);
  }
}
const ContextAll = React.createContext();
export default ContextAll;
