import React from "react";
import Navbar from "./components/Navbar";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import WhatsAppHome from "./pages/WhatsAppHome";
import { MuiThemeProvider } from "@material-ui/core";
import theme from "./theme/theme";
import ChatPages from "./pages/ChatPages";

function App() {
  return (
    <React.Fragment>
      <MuiThemeProvider theme={theme}>
        <BrowserRouter>
          <Navbar />
          <Switch>
            <Route exact path="/" component={WhatsAppHome} />
            <Route exact path="/chat" component={ChatPages} />
          </Switch>
        </BrowserRouter>
      </MuiThemeProvider>
    </React.Fragment>
  );
}

export default App;
